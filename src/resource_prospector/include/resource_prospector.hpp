#ifndef RPTOR_PLUGIN_HPP
#define RPTOR_PLUGIN_HPP

#include <ros/ros.h>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <map>
#include <ctime>
#include <string>
#include <sstream>
#include <cstdlib>
#include <stdexcept>
#include <stdlib.h>
#include "resource_prospector/displayRPTOR.h"
#include "resource_prospector/rState.h"
#include "resource_prospector/rProc.h"
#include "std_msgs/String.h"
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Odometry.h>
#include "lrpSubSystems.hpp"

namespace gazebo {
	
	class RPTOR;
	class RPTOR : public ModelPlugin 
	{
		public:		

		 RPTOR();
		~RPTOR() { }			
	
		resource_prospector::rState getRptorState();		
		resource_prospector::rProc getProcState();		
		void amclPosition(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg);
		void amclGoal(const geometry_msgs::PoseStamped::ConstPtr& msg);
		void driveSpeed(const nav_msgs::Odometry::ConstPtr& msg);
		void Load(physics::ModelPtr parent, sdf::ElementPtr sdf);
		void OnUpdate(const common::UpdateInfo &info);
	  bool displayRPTOR(resource_prospector::displayRPTOR::Request  &req,
		         				 resource_prospector::displayRPTOR::Response &res);

		private:
		int	sand;
		int	flip;
		float	pcsX;
		float	pcsY;
		float	pcsW;
		float	curGoalX;
		float	curGoalY;
		float difX;
    float difY;
    float distanceToNextWaypoint;
    float vLin;	
    float vAng;	    
		bool displaySwitch;
		bool resourceProspectorLatch;

		std::string name;
		std::string state;
		//LRPSUBSYSTEMS::cameraState camera;
		LRPSUBSYSTEMS::OnOff camera;
		LRPSUBSYSTEMS::Payload payload;
		LRPSUBSYSTEMS::StartStop mob;
		LRPSUBSYSTEMS::Kin kin_drive;
		LRPSUBSYSTEMS::KinSusp kin_susp;
		LRPSUBSYSTEMS::OnOff hazcams;
		LRPSUBSYSTEMS::OnOff motors;
		LRPSUBSYSTEMS::RoverMode mode;			
		LRPSUBSYSTEMS::GimbalState gimbals;
		LRPSUBSYSTEMS::Data navcam_data;
		physics::ModelPtr m_model;
		physics::WorldPtr m_world;
		sdf::ElementPtr m_sdf;
		event::ConnectionPtr m_updateConnection;
		ros::Publisher resourceProspectorStatus_pub;
		ros::Publisher resourceProspectorState_pub;
		ros::Subscriber odomSub;
		ros::Subscriber amclOdom; 
		ros::Subscriber amclOdom2; 
	  ros::ServiceServer resourceProspectorService;
	};
}

#endif