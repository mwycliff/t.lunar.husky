# q.lunar.husky

Husky w/out the arm.
  -- AMCL
 -- SLAM mapping
 -- G mapping
 -- Oven, Drill, Nss plugins
 -- Sim Helper Node
 -- Created July 27, 2018

 <!--<sig.mww.change.july.31.2018>-->


 			I alterered the src/husky/husky_gazebo/launch/playpen.launch
 			file 

    <!--<sig.mww.change.july.31.2018>-->
    <!--<arg name="gui" value="true"/>-->

    I changed a true value to false so that the gzclient
    will not start, and that way the machine's resources
    won't be bogged down.  You can type gzclient to get the
    gui to appear if you so desire.
