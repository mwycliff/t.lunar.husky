////////////  sig.mww.main.plugin  //////////////
/////////////////////////////////////////////////
///  UPDATED JULY 10  by  MATTHEW WYCLIFF     ///
///  UPDATED JULY 11  by  MATTHEW WYCLIFF     ///
///  UPDATED AUG  06  by  MATTHEW WYCLIFF     ///
/////////////////////////////////////////////////
#include "resource_prospector.hpp"
namespace gazebo 
{
	GZ_REGISTER_MODEL_PLUGIN(RPTOR)
		RPTOR::RPTOR()
		:	
		  sand(5),
		  flip(200),
			pcsX(0), 
			pcsY(0), 
			pcsW(0), 
			curGoalX(0), 
			curGoalY(0), 
	    difX(0),
	    difY(0),
	    vLin(0),
	    vAng(0),
	    distanceToNextWaypoint(0),
		  resourceProspectorLatch(true),
			m_model(),
			m_world(),
			m_sdf(),
			displaySwitch(true),
			name("RESOURCE PROSPECTOR"),
			state("STOWED"),
			gimbals(LRPSUBSYSTEMS::NOT_READY),
			camera(LRPSUBSYSTEMS::OFF),
			payload(LRPSUBSYSTEMS::Prospecting),
			mob(LRPSUBSYSTEMS::Stop),
      kin_drive(LRPSUBSYSTEMS::Hold),
      kin_susp(LRPSUBSYSTEMS::sHold),
      hazcams(LRPSUBSYSTEMS::OFF),
      motors(LRPSUBSYSTEMS::ON),
			mode(LRPSUBSYSTEMS::Stationary),
			navcam_data(LRPSUBSYSTEMS::NOT_FLOWING)
		{

			ROS_INFO_STREAM("\n\n\n\n...............................................\n"
			              <<"...............................................\n"  
			              <<"......                                   ........\n"  
			              <<"...... Attempting RPTOR	Initialization  ........\n"  
			              <<"......                                   ........\n"  
			              <<"...............................................\n"  
			              <<"...............................................\n\n\n\n");

			if (!ros::isInitialized())
			{
				ROS_FATAL_STREAM("\nRPTOR: not initialized, unable to load\n");
				return;
			}
		}

	  resource_prospector::rState RPTOR::getRptorState()
	  {
	  	resource_prospector::rState msg;		

	  	if(this->state=="OFF")
	  			msg.state=0;

	  	if(this->state=="ON")
	  			msg.state=1;

	  	if(this->state=="STOWED")
	  			msg.state=2;

	  	if(this->state=="DEPLOYED")
	  			msg.state=3;

	  	return msg;	
	  }	
		
		resource_prospector::rProc RPTOR::getProcState()
	  {
	  	resource_prospector::rProc msg;		
			msg.gimbals = (LRPSUBSYSTEMS::gimbalState2int(gimbals));
			//msg.cameraState = (LRPSUBSYSTEMS::cameraState2int(camera));
			msg.camera = (LRPSUBSYSTEMS::cameraState2int(camera));
			msg.payload = (LRPSUBSYSTEMS::payloadState2int(payload));
			msg.mob = (LRPSUBSYSTEMS::mobState2int(mob));
			msg.kin_drive = (LRPSUBSYSTEMS::kinDriveState2int(kin_drive));
			msg.kin_susp= (LRPSUBSYSTEMS::kinSuspState2int(kin_susp));
			msg.hazcams = LRPSUBSYSTEMS::hazcamState2int(hazcams);
			msg.mode = LRPSUBSYSTEMS::mode2int(mode);
			msg.motors = (LRPSUBSYSTEMS::wMotors2int(motors));
			msg.navcam_data = (LRPSUBSYSTEMS::navcamDataState2int(navcam_data));
			msg.LindriveSpeed = vLin;
			msg.AngdriveSpeed = vAng;
			msg.xPosition = pcsX;
			msg.yPosition = pcsY;			
			msg.distanceToNextWaypoint = distanceToNextWaypoint;
	  	return msg;	
	  }

		void RPTOR::amclPosition(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg)
		{	
			pcsX=msg->pose.pose.position.x;
			pcsY=msg->pose.pose.position.y;
			pcsW=msg->pose.pose.orientation.w;
		}

		void RPTOR::amclGoal(const geometry_msgs::PoseStamped::ConstPtr& msg)
		{	
			curGoalX = msg->pose.position.x;
			curGoalY = msg->pose.position.y;
		}		

		void RPTOR::driveSpeed(const nav_msgs::Odometry::ConstPtr& msg)
		{
			vLin = msg->twist.twist.linear.x;
			vAng = msg->twist.twist.angular.z;							
			if (vLin > -0.005 && vLin < 0.005 )	{
					vLin = 0;
					motors=LRPSUBSYSTEMS::OFF;
					hazcams=LRPSUBSYSTEMS::OFF;
			}

			else {
					motors=LRPSUBSYSTEMS::ON;
					hazcams=LRPSUBSYSTEMS::ON;
			}

			if (vAng > -0.005 && vAng < 0.005 )	{
					vAng = 0;
			} 
		}		

		bool RPTOR::displayRPTOR(resource_prospector::displayRPTOR::Request  &req,
				         				 resource_prospector::displayRPTOR::Response &res)
		{
		  displaySwitch = (req.RPTORdisplaySwitch); 
		  res.ackReceipt = true;
		  return true;
		}//RECIEVES.SERVICE.CALLS.FROM.NSS.PLUGIN

		void RPTOR::Load(physics::ModelPtr parent, sdf::ElementPtr sdf)
		{
				this->name =  "RESOURCE PROSPECTOR";
				this->state = "ON";
				this->mode = LRPSUBSYSTEMS::Stationary;
				this->motors = LRPSUBSYSTEMS::ON;
				this->navcam_data = LRPSUBSYSTEMS::NOT_FLOWING;
				this->gimbals = LRPSUBSYSTEMS::READY;
				this->camera = LRPSUBSYSTEMS::ON;
				this->payload = LRPSUBSYSTEMS::Prospecting;
				this->mob = LRPSUBSYSTEMS::Start;
        this->kin_drive = LRPSUBSYSTEMS::Cart;
 				this->kin_susp = LRPSUBSYSTEMS::sHold;
				this->hazcams = LRPSUBSYSTEMS::OFF;
				this->m_model = parent;
				this->m_world = parent->GetWorld();
				this->m_sdf   = sdf;
			  this->m_updateConnection = event::Events::ConnectWorldUpdateBegin(
			  boost::bind(&RPTOR::OnUpdate, this, _1));
				ROS_INFO_STREAM("\nRPTOR:  * * * * * *  Loaded  * * * * * *\n");
				ros::NodeHandle oNh;					
				resourceProspectorStatus_pub = oNh.advertise<std_msgs::String>("resourceProspectorStatus", 1000);			
				resourceProspectorState_pub = oNh.advertise<resource_prospector::rProc>("rProc", 1000);
		    resourceProspectorService = oNh.advertiseService("displayRPTOR", &RPTOR::displayRPTOR, this);
		    odomSub = oNh.subscribe("/husky_velocity_controller/odom",100, &RPTOR::driveSpeed, this);
		    amclOdom = oNh.subscribe("/amcl_pose",100, &RPTOR::amclPosition, this);
		    amclOdom2 = oNh.subscribe("/move_base/current_goal",100, &RPTOR::amclGoal, this);
		}//END RPTOR::LOAD
		
		void RPTOR::OnUpdate(const common::UpdateInfo &info)
		{ 	
			sand++;
			if (sand==flip)
			{				
		    std_msgs::String msg;
		    std::stringstream resourceProspectorStream;

		    resourceProspectorStream << "NAME:           "
		       << this->name
		       << "        ";

		    resourceProspectorStream << "STATE:     "
		       << this->state
		       << "";		       

		    msg.data = resourceProspectorStream.str();
		    resourceProspectorStatus_pub.publish(msg);  		    

		    if(displaySwitch==true) {
				    ROS_INFO("\r%s              ",msg.data.c_str());


		    		ROS_INFO(
		    		 "\rNAVCAM DATA:            %s                      "  
		    		 "\rGIMBALS:                %s                      "  
		         "\nCAMERA:                 %s  							"
		         "\nPAYLOAD:                %s"
		         "\nMOB:                    %s  					  \n"
		         "\nKINETIC DRIVE:          %s  							"
		         "\nKINETIC SUSPENSION:     %s  							"
		         "\nHAZARD CAMS:            %s                "
		         "\nMODE:                   %s                "
		         "\nMOTORS:                 %s              \n"
		         ,LRPSUBSYSTEMS::int2navcamDataStateString(LRPSUBSYSTEMS::navcamDataState2int(navcam_data)).c_str()
		         ,LRPSUBSYSTEMS::int2gimbalStateString(LRPSUBSYSTEMS::gimbalState2int(gimbals)).c_str()
		         ,LRPSUBSYSTEMS::int2cameraStateString(LRPSUBSYSTEMS::cameraState2int(camera)).c_str()
						 ,LRPSUBSYSTEMS::int2payloadStateString(LRPSUBSYSTEMS::payloadState2int(payload)).c_str()
		 				 ,LRPSUBSYSTEMS::int2mobStateString(LRPSUBSYSTEMS::mobState2int(mob)).c_str()
		 				 ,LRPSUBSYSTEMS::int2kinDriveStateString(LRPSUBSYSTEMS::kinDriveState2int(kin_drive)).c_str()
						 ,LRPSUBSYSTEMS::int2kinSuspStateString(LRPSUBSYSTEMS::kinSuspState2int(kin_susp)).c_str()
						 ,LRPSUBSYSTEMS::int2hazcamStateString(LRPSUBSYSTEMS::hazcamState2int(hazcams)).c_str()
						 ,LRPSUBSYSTEMS::int2modeString(LRPSUBSYSTEMS::mode2int(mode)).c_str()
						 ,LRPSUBSYSTEMS::int2wMotorsString(LRPSUBSYSTEMS::wMotors2int(motors)).c_str());

				    ROS_INFO("\rDRIVE SPEED:      Lin: [%.2f]   Ang: [%.2f]    "
						, vLin, vAng);
				    ROS_INFO("\rPOSITION:           X: [%.2f]     Y: [%.2f]    W: [%.2f]  "
				    ,pcsX,pcsY,pcsW);
				    ROS_INFO("\rGOAL:               X: [%.2f]     Y: [%.2f]               "
									,curGoalX,curGoalY);

				    difX = curGoalX - pcsX;
				    difX = curGoalY - pcsY;
						distanceToNextWaypoint=sqrt(pow(difX,2)+pow(difY,2));

				    ROS_INFO("\rDISTANCE FROM WAYPOINT:              [%.2f]               "
									,distanceToNextWaypoint);		 
		    }//switch.true.just.added.

				resource_prospector::rProc rpProcMsg;		
		  	rpProcMsg = getProcState();
		    resourceProspectorState_pub.publish(rpProcMsg);

 				ros::spinOnce();
		    sand = 0;
				}			
		}// end OnUpdate
}//END NAMESPACE GAZEBO