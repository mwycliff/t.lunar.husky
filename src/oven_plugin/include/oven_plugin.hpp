#ifndef OVEN_PLUGIN_HPP
#define OVEN_PLUGIN_HPP

#include <ros/ros.h>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <map>
#include <ctime>
#include <string>
#include <sstream>
#include <cstdlib>
#include <stdexcept>
#include <stdlib.h>
#include "oven_plugin/activateOVEN.h"
#include "oven_plugin/displayOVEN.h"
#include "oven_plugin/oState.h"
#include "std_msgs/String.h"

namespace gazebo {
	
	class OVEN;
	class OVEN : public ModelPlugin 
	{
		public:
		enum State { OFF=0, ON=1, STOWED=2, DEPLOYED=3 };
		 OVEN();
		~OVEN() { }			
		oven_plugin::oState getOvenState();		
		bool activateOVEN(oven_plugin::activateOVEN::Request  &req,
		         					oven_plugin::activateOVEN::Response &res);		
		bool displayOVEN(oven_plugin::displayOVEN::Request  &req,
		         				 oven_plugin::displayOVEN::Response &res);
		void Load(physics::ModelPtr parent, sdf::ElementPtr sdf);
		void OnUpdate(const common::UpdateInfo &info);

		private:
		int	sand;
		int	flip;
		int tmpA;
		float tmpB;
		bool tmpC;
		bool displaySwitch;
		bool ovenLatch;
		const char* tmpCC;
		std::string name;
		std::string state;		
		physics::ModelPtr m_model;
		physics::WorldPtr m_world;
		sdf::ElementPtr m_sdf;
		event::ConnectionPtr m_updateConnection;
		ros::Publisher ovenStatus_pub;
		ros::Publisher ovenState_pub;
	  ros::ServiceServer ovenService;
	  ros::ServiceServer ovenDisplay;
	};
}
#endif