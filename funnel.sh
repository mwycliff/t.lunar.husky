#!/bin/bash

tab="--tab"

one=""
onefish="bash -c 'roslaunch husky_gazebo mewsky_mewpen.launch';bash"

two=""
twofish="bash -c 'roslaunch husky_viz mew.launch';bash"

#roslaunch husky_navigation gmapping_demo.launch 

three=""
threefish="bash -c 'roslaunch husky_navigation mew.amcl_demo.launch';bash"

four=""
fourfish="bash -c 'rosrun nss_plugin simHelper';bash"

blue=""
bluefish="bash -c 'rostopic echo /rProc';bash"

red=""
redfish="bash -c 'rosservice call /queuePoint \"queueX: 7.0

queueY: 2.0\";rosservice call /queuePoint \"queueX: -6.0
queueY: 1.6\";rosservice call /queuePoint \"queueX: -4.0
queueY: 9.0\";rosservice call /queuePoint \"queueX: 7.0
queueY: -4.0\";rosservice call /queuePoint \"queueX: 5.4
queueY: 0.0\";rosservice call /queuePoint \"queueX: -3.0
queueY: 6.0\"';bash"


for i in 1; do
      one+=($tab -e "$onefish")         
done

gnome-terminal "${one[@]}"
sleep 6

#for i in 1; do
#      two+=($tab -e "$twofish")         
#done

#gnome-terminal "${two[@]}"
#sleep 6


#for i in 1; do
#      three+=($tab -e "$threefish")         
#done

#gnome-terminal "${three[@]}"
#sleep 6
#xdotool windowminimize $(xdotool getactivewindow)

#for i in 1; do
#      four+=($tab -e "$fourfish")         
#done

#gnome-terminal "${four[@]}"
#sleep 6





#for i in 1; do
#      blue+=($tab -e "$bluefish")         
#done
#
#gnome-terminal "${blue[@]}"
#sleep 6



#for i in 1; do
#      red+=($tab -e "$redfish")         
#done

#gnome-terminal "${red[@]}"
#xdotool windowminimize $(xdotool getactivewindow)
exit 0