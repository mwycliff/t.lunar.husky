///////////////////  sig.mww.  drill.plugin    f/////////////////////
#ifndef DRILL_PLUGIN_HPP
#define DRILL_PLUGIN_HPP

#include <ros/ros.h>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <map>
#include <string>
#include <stdlib.h>
#include <stdexcept>
#include <cstdlib>
#include <ctime>
#include "std_msgs/String.h"
#include <std_srvs/Empty.h>
#include <std_srvs/SetBool.h>
#include <geometry_msgs/Twist.h>
#include <sstream>
#include "drill_plugin/activateDrill.h"
#include "drill_plugin/displayDrill.h"
#include "drill_plugin/activateOVEN.h"

namespace gazebo {
	
	class DRILL;
	class DRILL : public ModelPlugin 
	{
		public:
		enum State { OFF=0, ON=1, STOWED=2, DEPLOYED=3 };
		 DRILL();
		~DRILL() {}
		void setactivateDrill(bool a);
		bool getactivateDrill();
		bool activateDrill(drill_plugin::activateDrill::Request &req,
		      						 drill_plugin::activateDrill::Response &res);
		void Load(physics::ModelPtr parent, sdf::ElementPtr sdf);
		void OnUpdate(const common::UpdateInfo &info);
		bool displayDrill(drill_plugin::displayDrill::Request  &req,
		         				 drill_plugin::displayDrill::Response &res);
		private:
		bool drillActivator;
		bool deployLatch;
		bool displaySwitch;
		int	sand;
		int	flip;
		int	drillTimer;
		std::string drillname;
		std::string drillstate;
		physics::ModelPtr m_model;
		physics::WorldPtr m_world;
		sdf::ElementPtr m_sdf;
		event::ConnectionPtr m_updateConnection;
		ros::Publisher drillSubscriber_pub;
	  ros::ServiceServer drillService1;
	  ros::ServiceServer drillService2;
	  
	};
}
#endif
