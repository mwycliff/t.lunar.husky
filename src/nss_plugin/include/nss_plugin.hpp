////////////  sig.mww.  nss.plugin    ///////////
/////////////////////////////////////////////////
///  UPDATED JUNE 20  by  MATTHEW WYCLIFF     ///
///  UPDATED JULY 09  by  MATTHEW WYCLIFF     ///
///  UPDATED JULY 10  by  MATTHEW WYCLIFF     ///
///  UPDATED JULY 11  by  MATTHEW WYCLIFF     ///
/////////////////////////////////////////////////

#ifndef NSS_PLUGIN_HPP
#define NSS_PLUGIN_HPP

#include <iostream>
#include <ros/ros.h>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <map>
#include <math.h>
#include <queue>
#include <string>
#include <stdlib.h>
#include <cstdlib>
#include "std_msgs/String.h"
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/Odometry.h>
#include <sstream>
#include <ctime>
#include "nss_plugin/TurnOnDetector.h"
#include "nss_plugin/activateDrill.h"
#include "nss_plugin/queuePoint.h"
#include "nss_plugin/popQueue.h"

namespace gazebo {
	
	class NSS;
	class NSS : public ModelPlugin 
	{
		public:
		enum State { OFF=0, ON=1, STOWED=2, DEPLOYED=3 };
		 NSS();
		~NSS() {}
		void setTonD(bool a);
		bool getTonD();
		bool TonD(nss_plugin::TurnOnDetector::Request  &req, nss_plugin::TurnOnDetector::Response &res);
		void Load(physics::ModelPtr parent, sdf::ElementPtr sdf);
		void OnUpdate(const common::UpdateInfo &info);

		private:
		bool nFlag;
		int	sand;
		int	flip;
		int	gateOne;
		int	gateTwo;
    const char* drlRqst;
		std::string nssname;
		std::string nssstate;
		physics::ModelPtr m_model;
		physics::WorldPtr m_world;
		sdf::ElementPtr m_sdf;
		event::ConnectionPtr m_updateConnection;
		ros::Publisher nssSubscriber_pub;
	  ros::ServiceServer service;
	};
}
#endif
