////////////  sig.mww.main.plugin  //////////////
/////////////////////////////////////////////////
///  UPDATED JULY 10  by  MATTHEW WYCLIFF     ///
///  UPDATED JULY 11  by  MATTHEW WYCLIFF     ///
///  UPDATED AUG  06  by  MATTHEW WYCLIFF     ///
/////////////////////////////////////////////////

#include <stdexcept>
#include <string>
#include "nss_plugin.hpp"
namespace gazebo 
{
	GZ_REGISTER_MODEL_PLUGIN(NSS)
		NSS::NSS()
		:	
			nFlag(true),
			sand(0),
			flip(200),
			gateOne(0),
			gateTwo(0),
			drlRqst("0"),			
			m_model(),
			m_world(),
			m_sdf(),
			nssname("Neutron Spectrometer System"),
			nssstate("STOWED")
		{
			ROS_INFO_STREAM("\n\n\n\n...............................................\n"
			              <<"...............................................\n"  
			              <<"......                                 ........\n"  
			              <<"...... Attempting NSS Initialization   .......\n"  
			              <<"......                                 ........\n"  
			              <<"...............................................\n"  
			              <<"...............................................\n\n\n\n");

			if (!ros::isInitialized())
			{
				ROS_FATAL_STREAM("\nNSS: not initialized, unable to load\n");
				return;
			}
		}//END.NSS.INITIALIZATION

		bool NSS::getTonD(){
			return nFlag;
		}
		void NSS::setTonD(bool a){
			nFlag = a;
		}
		
		bool NSS::TonD(nss_plugin::TurnOnDetector::Request  &req,
		         nss_plugin::TurnOnDetector::Response &res)
		{
		  setTonD(req.startDetecting);
		  res.ackReceipt = getTonD();
	  	ROS_INFO("\r                                                          "
	  					 "\n * * * SERVICE CALL FROM SIM HELPER NODE RECEIVED * * *");
		  return true;
		}//RECIEVES.REQUESTS.AND.SENDS.RESPONSES.TO.AND.FROM.SIM.HELPER.NODE

		void NSS::Load(physics::ModelPtr parent, sdf::ElementPtr sdf)
		{
				this->m_model = parent;
				this->m_world = parent->GetWorld();
				this->m_sdf   = sdf;
				this->nssname =  "NSS";
				this->nssstate = "STOWED";
			  this->m_updateConnection = event::Events::ConnectWorldUpdateBegin(
			  boost::bind(&NSS::OnUpdate, this, _1));
				ROS_INFO_STREAM("\nNSS:  * * * * * *  Loaded  * * * * * *\n");

				ros::NodeHandle dNhOne;//xdNhOne==DRILL.NODE.HANDLE.ONE					
				nssSubscriber_pub = dNhOne.advertise<std_msgs::String>("nssSubscriber", 2000);
			  service = dNhOne.advertiseService("TonD", &NSS::TonD, this);
				ROS_INFO_STREAM("\r                                             \n"
			              <<"     ...................................         \n"  
			              <<"     .                                 .         \n"  
			              <<"     .           NSS LOADED            .         \n"  
			              <<"     .                                 .         \n"  
			              <<"     ...................................         \n"  
			              <<"                                                   ");
		 }//END NSS::LOAD
		
		void NSS::OnUpdate(const common::UpdateInfo &info)
		{ 		
			sand++;
			if (sand==flip)
			{
				ROS_INFO_STREAM("\r                                                \n"
			              <<"     ...................................            \n"  
			              <<"     .         NSS UPDATED             .            \n"  
			              <<"     ...................................            \n"  
			              <<"                                                      ");

		    std_msgs::String msg;
		    std::stringstream cc;

		    cc << "[NAME:   "
		       << this->nssname
		       << "]  ";

		    cc << "[STATE:   "
		       << this->nssstate
		       << "]";
	
				if (getTonD()==true)
			  {	
					  cc << "  [DETECTOR:   OFF" << "]  ";
				    this->nssstate = "STOWED"; 
					  gateOne=0;
					  gateTwo=0; 				  
				}
				else
				{	
				    cc << "  [DETECTOR:   ON" << "]  ";
				    this->nssstate = "DEPLOYED"; 
				    gateOne++;
				    gateTwo++;
				}
				    
		    msg.data = cc.str();
		    nssSubscriber_pub.publish(msg);  
		    ROS_INFO("\r  %s                                         "
		    	       ,msg.data.c_str());

				ros::NodeHandle dNhTwo;
				ros::ServiceClient drillClient = dNhTwo.serviceClient<nss_plugin::activateDrill>("activateDrill");  
				nss_plugin::activateDrill srv;
  
		    //if (gateOne==2)
		    if (gateOne > 2 && gateTwo < 4)
			  {	
				  srv.request.startDrilling = atoll(drlRqst);
				  (drillClient.call(srv));
					drlRqst = "1";
	  		}//end.big.if.	
				else;
				drlRqst = "0";
 				ros::spinOnce();
		    sand = 0;
				} // END HOURGLASS.TIME.PERIOD.MODULATOR.SAND.FLIP.DEVICE.	
				else;
				}// end OnUpdate
	}//END NAMESPACE GAZEBO
