////////////  sig.mww.main.plugin  //////////////
/////////////////////////////////////////////////
///  UPDATED JULY 11  by  MATTHEW WYCLIFF     ///
///  UPDATED AUG  06  by  MATTHEW WYCLIFF     ///
///  UPDATED AUG  13  by  MATTHEW WYCLIFF     ///
/////////////////////////////////////////////////

#include "oven_plugin.hpp"
namespace gazebo 
{
	GZ_REGISTER_MODEL_PLUGIN(OVEN)
		OVEN::OVEN()
		:	
		  sand(4),
		  flip(200),
		  ovenLatch(true),
			m_model(),
			m_world(),
			m_sdf(),
			tmpA(0),
			tmpB(2),
			tmpC(false),
			displaySwitch(true),
			tmpCC("false"),
			name("Oxygen & Volatile Extraction Node"),
			state("STOWED")

		{
			ROS_INFO_STREAM("\n\n\n\n...............................................\n"
			              <<"...............................................\n"  
			              <<"......                                 ........\n"  
			              <<"...... Attempting OVEN	Initialization  ........\n"  
			              <<"......                                 ........\n"  
			              <<"...............................................\n"  
			              <<"...............................................\n\n\n\n");

			if (!ros::isInitialized())
			{
				ROS_FATAL_STREAM("\nOVEN: not initialized, unable to load\n");
				return;
			} 
		}

	  oven_plugin::oState OVEN::getOvenState()
	  {
	  	oven_plugin::oState msg;		

	  	if(this->state=="OFF")
	  			msg.state=0;

	  	if(this->state=="ON")
	  			msg.state=1;

	  	if(this->state=="STOWED")
	  			msg.state=2;

	  	if(this->state=="DEPLOYED")
	  			msg.state=3;

	  	return msg;	
	  }	

		bool OVEN::activateOVEN(oven_plugin::activateOVEN::Request  &req,
		         							 oven_plugin::activateOVEN::Response &res)
		{
		  tmpB = (req.rSampleSize); 
		  tmpC = (req.throwFlag); 
		  if ((req.throwFlag)==true);
		  tmpCC = "true"; 
		  res.ackReceipt = true;
		  return true;
		}//RECIEVES.SERVICE.CALLS.FROM.NSS.PLUGIN

		bool OVEN::displayOVEN(oven_plugin::displayOVEN::Request  &req,
		         							 oven_plugin::displayOVEN::Response &res)
		{
		  displaySwitch = (req.OvenDisplaySwitch); 
		  res.ackReceipt = true;
		  return true;
		}//RECIEVES.SERVICE.CALLS.FROM.NSS.PLUGIN

		void OVEN::Load(physics::ModelPtr parent, sdf::ElementPtr sdf)
		{
				this->m_model = parent;
				this->m_world = parent->GetWorld();
				this->m_sdf   = sdf;
				this->name =  "OVEN";
				this->state = "STOWED";
			  this->m_updateConnection = event::Events::ConnectWorldUpdateBegin(
			  boost::bind(&OVEN::OnUpdate, this, _1));
				ROS_INFO_STREAM("\nOVEN:  * * * * * *  Loaded  * * * * * *\n");
				ros::NodeHandle oNh;					
				ovenStatus_pub = oNh.advertise<std_msgs::String>("ovenStatus", 1000);			
				ovenState_pub = oNh.advertise<oven_plugin::oState>("oState", 1000);
		    ovenService = oNh.advertiseService("activateOVEN", &OVEN::activateOVEN, this);
		    ovenDisplay = oNh.advertiseService("displayOVEN", &OVEN::displayOVEN, this);

		}//END OVEN::LOAD
		
		void OVEN::OnUpdate(const common::UpdateInfo &info)
		{ 		
			sand++;
			if (sand==flip)
			{
				if (displaySwitch==true) {
				ROS_INFO_STREAM("\r                                                  \n"
			              <<"     ...................................              \n"  
			              <<"     .         OVEN UPDATED            .              \n"  
			              <<"     ...................................              \n"  
			              <<"                                                        ");				
				}//end.if
		    std_msgs::String msg;
		    std::stringstream ovenStream;

		    ovenStream << "[NAME:   "
		       << this->name
		       << "]  ";

		    ovenStream << "[STATE:   "
		       << this->state
		       << "]                 ";

		    msg.data = ovenStream.str();
		    ovenStatus_pub.publish(msg);  
		    if (displaySwitch==true) {
		    ROS_INFO("\r%s     "
		    	,msg.data.c_str());
		  	}

				oven_plugin::oState ovStateMsg;		
		  	ovStateMsg = getOvenState();
		    ovenState_pub.publish(ovStateMsg);

				if (tmpA>=18 && tmpA < 24)
			  {//THIS CODE WILL EXECUTE AFTER THE DRILL FLUTE CONTENTS HAVE BEEN EVALUATED
								  	
			  		float e1,e2,e3,e4,e5,e6;		
			  		e1=(1/tmpB)*.1+2.7;
			  		e2=(2/tmpB)*.05+.1;
			  		e3=(3/tmpB)*.1+.8;
			  		e4=(4/tmpB)*.1+.12;
			  		e5=(5/tmpB)*.1+.2;
			  		e6=(6/tmpB)*.1+1.20;
			  		
				  	if (displaySwitch==true) {
				  	ROS_INFO("\r                                                          "
		  					 "\n    H20: [%.3f]%%     CO: [%.3f]%%     C02: [%.3f]%%           \n"
		  					 "\n    H2: [%.3f]%%     H2S: [%.3f]%%     NH3: [%.3f]%%           \n"
		  					 "\n        CHEMICAL ANALYSIS COMPLETE                 [%d] < 24   \n"
		  					 ,e1,e2,e3,e4,e5,e6,tmpA);
				  	}

					  tmpA++;
					  tmpCC="false";

						if (tmpA==21)
						{
							this->state = "OFF";				  	
							ovStateMsg = getOvenState();
		    			ovenState_pub.publish(ovStateMsg);	
		    			this->state = "STOWED";
						}

						if (tmpA >= 24)
						{		
					    		 tmpA=0;
					    		 ovenLatch=true;
						}    
				}
				else;		 
				if (this->state == "ON" && tmpA>=10)
			  {
			  		if (tmpA%2!=0)
					  {
					  	if (displaySwitch==true) {
					  	ROS_INFO("\r  *** BAKING THE SAMPLE ***   -->  [%d] DEGREES CELSIUS\n",tmpA*13  );
					  	}
					  	tmpA++;
						}
						else
						{
							if (displaySwitch==true) {
					  	ROS_INFO("\r                              -->  [%d] DEGREES CELSIUS\n",tmpA*13  );
					  	}
					  	tmpA++;
						}
				}
				if (tmpCC=="true" && ovenLatch == false && tmpB > 30 &&tmpA == 10)
			  {	
			  	if (displaySwitch==true) {
			  	ROS_INFO("\r                                                     "
			  					 "\n    MESSAGE: *** WEIGHT OF SAMPLE IS SUFFICIENT ***\n"
			  					 "\n                                                   \n"  
			  					 "\n          *** COMMENCING BAKING PROCESS ***        \n");
			  	}
					this->state = "ON";
					tmpA++;
				}
				else if (tmpCC=="true" && ovenLatch == false && tmpB <= 30 &&tmpA == 10)
			  {	
			  	if (displaySwitch==true) {
			  	ROS_INFO("\r                                                     "
			  					 "\n    MESSAGE: *** WEIGHT OF SAMPLE IS DEFICIENT  ***\n"
			  					 "\n                                                   \n"  
			  					 "\n                *** DUMPING SAMPLE ***             \n");
			  	}

				  tmpCC="false";
				  ovenLatch=true;
					this->state = "OFF";							
		  		ovStateMsg = getOvenState();
		    	ovenState_pub.publish(ovStateMsg);
					this->state = "STOWED";		
					tmpA=0;
				}
				else;		    
				if (tmpCC=="true" && ovenLatch == true)
			  {
			  	if (displaySwitch==true) {
			  	ROS_INFO("\r                                                          "
			  					 "\n\n  *** WEIGHING SAMPLE ***  --->  [%d] [%.1f] grams.\n",tmpA,tmpB);
			  	}

				this->state = "DEPLOYED";
				ovenLatch = false;
				tmpA=10;
				}
				else;
 				ros::spinOnce();
		    sand = 0;
				}	
				else;		
				}// end OnUpdate
}//END NAMESPACE GAZEBO