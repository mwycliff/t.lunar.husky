///////////////////  sig.mww.  drill.plugin    f/////////////////////
#ifndef SIMHELPER_HPP
#define SIMHELPER_HPP

#include <iostream>
#include <ros/ros.h>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <map>
#include <math.h>
#include <queue>
#include <string>
#include <stdlib.h>
#include <cstdlib>
#include "std_msgs/String.h"
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <sstream>
#include <ctime>
#include "nss_plugin/TurnOnDetector.h"
#include "nss_plugin/activateDrill.h"
#include "nss_plugin/queuePoint.h"
#include "nss_plugin/popQueue.h"
#include "nss_plugin/oState.h"
#include "nss_plugin/cancelRoute.h"
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

struct xYpoint
{
	float xX;
	float yY;
	xYpoint();
	xYpoint(float X, float Y)
	{
		xX=X;
		yY=Y;
	}
	~xYpoint(){}
};

class accRange;//acceptable.Range.class.
class accRange
{	
	private:
		float pcsX;
		float pcsY;
		float pcsW;
		float coordX;
		float coordY;		
		float X;
		float Y;
		float Xarg;
		float Yarg;
		float Warg;
		float currentOrientation;
		bool hgFlip;
		bool popLatch;
		bool navLatch;
		bool brandNewFlag;
		bool brandNewFlagB;
		bool navStackState;
		float radius;
		bool cond;
		char* currentOvenState;
		std::queue <xYpoint> mP;
		MoveBaseClient ac;
		const char* flag;

	public: 
		float tmpX;
		float tmpY;
		float tmpXbeta;
		float tmpYbeta;
		int queueSize;	
		int hgSand;	
		const char* nssDetectorLatch;
		accRange();
		~accRange() { }
		void simHelperCallback(const nav_msgs::Odometry::ConstPtr& msg);
		void setX(float a);
		float getX();
		void setCond(bool a);
		bool getCond();
		void hourGlass(bool a);//sig.mww.justadded	
		void qNavStack(float Xarg, float Yarg, float Warg); 
		void inRange(const nav_msgs::Odometry::ConstPtr& msg);
		void enqueue(const nav_msgs::Odometry::ConstPtr& msg);
		void setFlag(const char* a);	

		void setcW(float a);
		float getcW();
		
		const char* getFlag();
		void generateMbClient();
		bool queueCoord(nss_plugin::queuePoint::Request &req,
		         							 nss_plugin::queuePoint::Response &res);
		bool popQueue(nss_plugin::popQueue::Request &req,
		         							 nss_plugin::popQueue::Response &res);
		bool cancelRoute(nss_plugin::cancelRoute::Request &req,
		         							 nss_plugin::cancelRoute::Response &res);
		void ovenStateCallback(const nss_plugin::oState::ConstPtr& oStateMsg);
		void amclPosition(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg);
};//end.class.accRange.definition
#endif