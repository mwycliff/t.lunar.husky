///////////////////  sig.mww.  drill.plugin    f/////////////////////
#ifndef DRILL_PLUGIN_HPP
#define DRILL_PLUGIN_HPP

#include "ros/ros.h"
#include <ros/ros.h>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <map>
#include <string>
#include <stdlib.h>
#include <cstdlib>
#include "std_msgs/String.h"
#include <geometry_msgs/Twist.h>
#include <sstream>
#include <ctime>
#include "drill_plugin/activateOVEN.h"

namespace gazebo {
	
	class DRILL;
	class DRILL : public ModelPlugin 
	{
		public:
		 DRILL();
		~DRILL() {}


		void setactivateDrill(bool a);
		bool getactivateDrill();


		void setdRando(int a);
		int getdRando();
		void setDelMe(bool a);
		bool getDelMe();

		//void activateDrillCallback(const std_msgs::String::ConstPtr& msg);

		//void DelMeCallback(const std_msgs::String::ConstPtr& msg);//FACILITATES.DELME.SERVICE
		void Load(physics::ModelPtr parent, sdf::ElementPtr sdf);
		void OnUpdate(const common::UpdateInfo &info);

		private:
		bool dFlag;
		int dRando;
		int	sand;
		int	flip;
		std::string drillname;
		std::string drillstate;
		physics::ModelPtr m_model;
		physics::WorldPtr m_world;
		sdf::ElementPtr m_sdf;
		event::ConnectionPtr m_updateConnection;
		ros::Publisher drillSubscriber_pub;
	  //ros::Subscriber celery;
	  ros::ServiceServer service;
	  ros::ServiceServer drillService;
	};
}
#endif
