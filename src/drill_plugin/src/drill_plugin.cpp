/////////  sig.mww.  drill.plugin  //////////////
/////////////////////////////////////////////////
///  UPDATED JUNE 20  by  MATTHEW WYCLIFF     ///
///  UPDATED JULY 09  by  MATTHEW WYCLIFF     ///
///  UPDATED JULY 10  by  MATTHEW WYCLIFF     ///
///  UPDATED JULY 11  by  MATTHEW WYCLIFF     ///
/////////////////////////////////////////////////

#include "drill_plugin.hpp"

namespace gazebo 
{
	GZ_REGISTER_MODEL_PLUGIN(DRILL)
		DRILL::DRILL()
		:				
			drillActivator(true),
			deployLatch(true),
			sand(2),
			flip(200),
			drillTimer(0),
			displaySwitch(true),
			m_model(),
			m_world(),
			m_sdf(),
			drillname("Neutron Spectrometer System"),
			drillstate("STOWED")
		{
			ROS_INFO_STREAM("\n\n\n\n...............................................\n"
			              <<"...............................................        \n"  
			              <<"......                                 ........        \n"  
			              <<"...... Attempting DRILL Initialization ........        \n"  
			              <<"......                                 ........        \n"  
			              <<"...............................................        \n"  
			              <<"...............................................\n\n\n\n");

			if (!ros::isInitialized())
			{
				ROS_FATAL_STREAM("\nDRILL: not initialized, unable to load\n");
				return;
			}
		}//END.DRILL.INITIALIZATION

		void DRILL::setactivateDrill(bool a){
			drillActivator = a;
		}

		bool DRILL::getactivateDrill(){
			return drillActivator;
		}

		bool DRILL::activateDrill(drill_plugin::activateDrill::Request  &req,
		         drill_plugin::activateDrill::Response &res)
		{
		  setactivateDrill(req.startDrilling);
		  res.ackReceipt = getactivateDrill();
		  return true;
		}//RECIEVES.SERVICE.CALLS.FROM.NSS.PLUGIN

		bool DRILL::displayDrill(drill_plugin::displayDrill::Request  &req,
				         				 drill_plugin::displayDrill::Response &res)
		{
		  displaySwitch = (req.DrillDisplaySwitch); 
		  res.ackReceipt = true;
		  return true;
		}//RECIEVES.SERVICE.CALLS.FROM.NSS.PLUGIN
		
		void DRILL::Load(physics::ModelPtr parent, sdf::ElementPtr sdf)
		{
				this->m_model = parent;
				this->m_world = parent->GetWorld();
				this->m_sdf   = sdf;
				this->drillname =  "DRILL";
				this->drillstate = "STOWED";
			  this->m_updateConnection = event::Events::ConnectWorldUpdateBegin(
			  boost::bind(&DRILL::OnUpdate, this, _1));
				ROS_INFO_STREAM("\nDRILL:  * * * * * *  Loaded  * * * * * *\n");
				ros::NodeHandle dNh;					
				drillSubscriber_pub = dNh.advertise<std_msgs::String>("drillSubscriber", 2000);// DELETE ME
				drillService1 = dNh.advertiseService("activateDrill", &DRILL::activateDrill, this);
				drillService2 = dNh.advertiseService("displayDrill", &DRILL::displayDrill, this);
		 }//END DRILL::LOAD

		void DRILL::OnUpdate(const common::UpdateInfo &info)
		{ 		
			sand++;
			if (sand==flip)
			{
				
			if (displaySwitch==true) {
				ROS_INFO_STREAM("\r                                                \n"
			              <<"     ...................................            \n"  
			              <<"     .         DRILL UPDATED           .            \n"  
			              <<"     ...................................            \n"  
			              <<"                                                      ");
			}

		    std_msgs::String msg;
		    std::stringstream cc;

		    cc << "[NAME:   "
		       << this->drillname
		       << "]  ";

		    cc << "[STATE:   "
		       << this->drillstate
		       << "]";

		    msg.data = cc.str();
		    drillSubscriber_pub.publish(msg);  
		    if (displaySwitch==true) {
		    ROS_INFO("\r %s                                        ", msg.data.c_str());
		  }

				if (getactivateDrill()==false&&deployLatch==true)
			  {	
			  	ROS_INFO("\r                                                          "
			  					 "\n    MESSAGE: *** DEPLOYING DRILL ***\n");

					this->drillstate = "DEPLOYED";
			  	deployLatch=false; //LATCH.CLICKS
				}

				//if (getactivateDrill()==false)
				if (getactivateDrill()==false && this->drillstate == "DEPLOYED")
			  {	
			  	ROS_INFO("\r                                                          "
			  					 "\n\t\t         DRILLING IN PROGRESS"); 
			  	drillTimer++; //TIMER INCREMENTS EACH UPDATE UNTIL IT GETS TO FIVE			  	
				}

				if (drillTimer > 4) //DRILL DEACTIVATES AFTER TIMER REACHES FIVE
			  {	
			  	ROS_INFO("\r               * * * DEACTIVATING DRILL * * *         ");
			  	ROS_INFO("\r                                                      "
			  					 "\n* * * TRANSFERING SAMPLE TO OVEN CRUCIBLE FOR EVALUATION * * *");

			  	setactivateDrill(true); //RESET.DRILL.ACTIVATOR

			  	deployLatch=true; // RESET.LATCH

			  	drillTimer = 0; //RESET.TIMER

					this->drillstate = "STOWED";

					ros::NodeHandle dNh;					
					ros::ServiceClient drillClient = dNh.serviceClient<drill_plugin::activateOVEN>("activateOVEN");  
					drill_plugin::activateOVEN srv;

  			std::srand(time(0));  //RANDOM NUMBER GENERATOR IS SEEDED			
			  int JJJ = (rand() % 100);
			  std::stringstream strs;
			  strs << JJJ;
			  std::string temp_str = strs.str();
			  const char* J = (const char*) temp_str.c_str();

			  srv.request.rSampleSize = atoll(J);
			  srv.request.throwFlag = atoll("true");
			  drillClient.call(srv);

				}
				else;

 				ros::spinOnce();
		    sand = 0;
				}	
				else;		
				}// end OnUpdate
	}//END NAMESPACE GAZEBO