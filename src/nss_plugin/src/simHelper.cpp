///////////  sig.mww.simHelper  /////////////////
/////////////////////////////////////////////////
///  UPDATED JULY 19  by  MATTHEW WYCLIFF     ///
///  UPDATED JULY 30  by  MATTHEW WYCLIFF     ///
/////////////////////////////////////////////////
#include "simHelper.hpp"

accRange::accRange()
	: 
		pcsX(0), 
		pcsY(0), 
		pcsW(0), 
		coordY(99),
		tmpX(1),
		tmpY(1),
		tmpXbeta(1),
		tmpYbeta(1),
		X(0),
		Y(0),
		Xarg(0),
		Yarg(0),
		Warg(0),
		hgFlip(false),
		popLatch(false),
		navLatch(false),
		brandNewFlag(false),
		brandNewFlagB(true),
		queueSize(1),
		hgSand(0),
		radius(0),
		navStackState(0),
		flag("0"),
		nssDetectorLatch("0"),
		ac("move_base", true),
		currentOvenState(),
		currentOrientation(0),
		cond(false)
{
	ROS_INFO("\n\n accRange class has been instantiated \n"); 
}

void accRange::setX(float a){
	X = a;
}

float accRange::getX(){
	return X;
}

void accRange::setCond(bool a){
	cond = a;
}

bool accRange::getCond(){
	return cond;
}

void accRange::setFlag(const char* a){
	flag = a;
}

const char* accRange::getFlag(){
	return flag;
}

void accRange::setcW(float a){
	currentOrientation = a;
}

float accRange::getcW(){
	return currentOrientation;
}

void accRange::hourGlass(bool a)
{	
	if (a == true)
		hgSand++;	

	if (hgSand > 399 && currentOvenState=="OFF")
	{
		mP.pop();
		brandNewFlag=false;
		brandNewFlagB=true;
		navLatch = false;
		hgSand=0;
		hgFlip=false;
	}	
}

void accRange::ovenStateCallback(const nss_plugin::oState::ConstPtr& oStateMsg)
{
	  	if(oStateMsg->state == 0)
	  			currentOvenState="OFF";
	  			
	  	if(oStateMsg->state == 1)
	  			currentOvenState="ON";

	  	if(oStateMsg->state == 2)
	  			currentOvenState="STOWED";

	  	if(oStateMsg->state == 3)
	  			currentOvenState="DEPLOYED";
}
			
void accRange::qNavStack(float Xarg, float Yarg, float Warg)
{
  while(!ac.waitForServer(ros::Duration(3.0)))
  {
    ROS_INFO("Waiting for the move_base action server to come up");
  }  
  move_base_msgs::MoveBaseGoal goal;
  goal.target_pose.header.frame_id = "map";
  goal.target_pose.header.stamp = ros::Time::now();
  goal.target_pose.pose.position.x = Xarg;
  goal.target_pose.pose.position.y = Yarg;
  goal.target_pose.pose.orientation.w = Warg;
  ROS_INFO("Sending goal");
  ac.sendGoal(goal);
}//END.IMPLEMENTATION.QNAV.STACK 

void accRange::inRange(const nav_msgs::Odometry::ConstPtr& msg)
{		
 	if (mP.empty()!=true &&
 		( ac.getState() == actionlib::SimpleClientGoalState::ABORTED
		||ac.getState() == actionlib::SimpleClientGoalState::REJECTED
		||ac.getState() == actionlib::SimpleClientGoalState::RECALLED))
		
 	{	
		 		mP.pop();
				tmpXbeta=tmpX=mP.front().xX;	
				tmpYbeta=tmpY=mP.front().yY;	
				queueSize--;
		 		while(!ac.waitForServer(ros::Duration(2.0)))
			  {
			    ROS_INFO("\r\n          . , _ o 0 O  SETTING NEW GOAL  O 0 o _ , .              \n");
			  }
				ac.cancelAllGoals();
		 		brandNewFlagB=false;
				navLatch = false;
				hgSand=0;
				hgFlip=false;
				brandNewFlag=false;
 	}	

	hourGlass(hgFlip);
				
	if(mP.empty()!=true)
	{
		if (navLatch==false)
		{	
	    qNavStack(mP.front().xX, mP.front().yY, 1);
	    while(!ac.waitForServer(ros::Duration(2.0)))
		  {
		    ROS_INFO("\r\n          . , _ o 0 O  SETTING NEW GOAL  O 0 o _ , .              \n");
		  }
	    navLatch = true; 
  	}

		X=mP.front().xX - pcsX;//X.DIFFERENCE
		Y=mP.front().yY - pcsY;//Y.DIFFERENCE 
		radius=sqrt(pow(X,2)+pow(Y,2));

		ROS_INFO("\rRadius:    <%.2f>     Point of Interest  <%.1f,%.1f>          \n"
		         ,radius,mP.front().xX,mP.front().yY);
		ROS_INFO("\rmPback:    <%.1f,%.1f>    mPfront: <%.1f,%.1f>    queueSize:  %d"
	           "                                                        \n\n"
	           ,mP.back().xX,mP.back().yY,mP.front().xX,mP.front().yY,mP.size());	

		cond=(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED);

		if(cond==1)
		{	
			ROS_INFO("\r                                                       \n"
				       "         IN RANGE OF POI:   <%.1f,%.1f>                \n\n"
				       ,mP.front().xX,mP.front().yY); 

			setFlag("0");
			hgFlip=true;
  	}
		
		else
		{	
			ROS_INFO("\r                                                            \n" 
				       "            . . . %.3f meters from target . . .             \n\n",radius); 
			setFlag("1");
		}

		}
		else
		{
			X=coordX-pcsX;
			Y=coordY-pcsY; 
			radius=sqrt(pow(X,2)+pow(Y,2));
			cond=(radius<5);

			ROS_INFO("\rRadius:    <%.2f>     Point of Interest  <%.1f,%.1f>        \n"
			         ,radius,coordX,coordY);

			ROS_INFO("\rmPback: <%c,%c>    mPfront: <%c,%c>    queueSize: %d      "
		           "\n                                                          "
		          	,'-','-','-','-',mP.size());

			if(getCond()==1)
			{	
				ROS_INFO("\r                                                    \n\n"
					       "               ADD A POINT TO THE QUEUE               \n\n"); 
			}
			else
			{	
				ROS_INFO("\r                                                    \n\n"
					       "                   QUEUE UP A POINT                   \n\n"); 
				setFlag("1");
			}
		}
}//END.MEMBER.FXN.IMPLEMENTATION

void accRange::enqueue(const nav_msgs::Odometry::ConstPtr& msg)
{	
		if ( tmpX != tmpXbeta || tmpY != tmpYbeta )//CHECKS.FOR.PARITY
		{			
				{
					ROS_INFO("\r                                               \n\n"
					    	"              COORD QUEUED CALLED         						 \n" 
					    	"              COORD QUEUED CALLED                       "); 
				}

				xYpoint* baker = new xYpoint(tmpX,tmpY);
				mP.push(*baker);
				tmpXbeta=tmpX;	
				tmpYbeta=tmpY;	
				queueSize++;
		}//END.INNER.MAIN.IF	
}//END.MEMBER.FXN.IMPLEMENTATION.ENQUEUE

void accRange::simHelperCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
	//ROS_INFO("\rPOSITION    x: [%.2f], y: [%.2f],  w: [%.2f]                          \n"
	ROS_INFO("\r                                                                           \n"
					 "POSITION             x: [%.1f]  y: [%.1f]                                    \n"
					, pcsX, pcsY );

	ROS_INFO("\rPOINT OF INTEREST    x: [%.1f]  y: [%.1f]       DISTANCE FROM POI:  %.2f   \n"
					, mP.front().xX, mP.front().yY, radius );

	ROS_INFO("\rVELOCITY          Linear: [%.2f]  Angular: [%.2f]                          \n"
					, msg->twist.twist.linear.x,msg->twist.twist.angular.z);
}

void accRange::amclPosition(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg)
{	
	pcsX=msg->pose.pose.position.x;
	pcsY=msg->pose.pose.position.y;
	pcsW=msg->pose.pose.orientation.w;
}
//////////////////////////////////////////////////////////////////////////////////

bool accRange::queueCoord(nss_plugin::queuePoint::Request &req,
         							 nss_plugin::queuePoint::Response &res)
{
	tmpX = (req.queueX);
  tmpY = (req.queueY); 
  return true;
}//RECIEVES.SERVICE.CALLS

bool accRange::popQueue(nss_plugin::popQueue::Request &req,
         							 nss_plugin::popQueue::Response &res)
{			
	popLatch = (req.popper);		
	if (popLatch == true)
	{
		mP.pop();		
	}	
	else;
	popLatch=false;
  res.ackReceipt = true;
  return true;
}//RECIEVES.SERVICE.CALLS

bool accRange::cancelRoute(nss_plugin::cancelRoute::Request &req,
         							 nss_plugin::cancelRoute::Response &res)
{	
	if (req.abort == 1)
	{
			mP.pop();
			tmpXbeta=tmpX=mP.front().xX;	
			tmpYbeta=tmpY=mP.front().yY;	
			queueSize--;
	 		while(!ac.waitForServer(ros::Duration(2.0)))
		  {
		    ROS_INFO("\r\n          . , _ o 0 O  SETTING NEW GOAL  O 0 o _ , .              \n");
		  }
			ac.cancelAllGoals();
			navLatch = false;
			hgSand=0;
			hgFlip=false;
			brandNewFlag=false;
			brandNewFlagB=true;
		  res.ackReceipt = 1;
	}
  return true;
}//RECIEVES.SERVICE.CALLS
		
//////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv)
{
	ros::init(argc, argv, "simHelper");
  ros::NodeHandle nh;
	accRange art;
	ros::Subscriber odomSub4 = nh.subscribe("/amcl_pose",100, &accRange::amclPosition, &art);
	ros::Subscriber odomSub1 = nh.subscribe("/husky_velocity_controller/odom",100, &accRange::simHelperCallback, &art);
	ros::Subscriber odomSub3 = nh.subscribe("/husky_velocity_controller/odom",100, &accRange::enqueue, &art);  
	ros::Subscriber odomSub2 = nh.subscribe("/husky_velocity_controller/odom",100, &accRange::inRange, &art);  //just.added	
	ros::Subscriber ovenState_sub = nh.subscribe("oState", 100, &accRange::ovenStateCallback, &art);
	ros::ServiceClient nssClient = nh.serviceClient<nss_plugin::TurnOnDetector>("TonD");  
	ros::ServiceServer simHelperService1 = nh.advertiseService("queuePoint", &accRange::queueCoord, &art);
	ros::ServiceServer simHelperService2 = nh.advertiseService("popQueue", &accRange::popQueue, &art);
	ros::ServiceServer simHelperService3 = nh.advertiseService("cancelRoute", &accRange::cancelRoute, &art);
  nss_plugin::TurnOnDetector srv;

  ros::Rate loop_rate(5);
	while (ros::ok())
  {
		if (art.getFlag()!=art.nssDetectorLatch) // Once the robot is in an acceptable range of a waypoint
		{
				  srv.request.startDetecting = atoll(art.getFlag()); // A boolean value is passed in the form of a char -> "1"
				  if (nssClient.call(srv)) // A service call is sent to the NSS Element Detector
				  {
				  		ROS_INFO("\r                                               \n\n"
				    	"              SERVICE CALLED         "); 
				  }
				  else
				  {
				    ROS_ERROR("Failed to call service throw the boolean flag");
				    return 1;
				  }
				art.nssDetectorLatch=art.getFlag();
		}//end.big.if.	
		else;	
    ros::spinOnce();
    loop_rate.sleep();
  }//END.WHILE.LOOP.(ROS==OK)
  ros::spin();
  return 0;
}