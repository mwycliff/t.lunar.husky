#ifndef LRPSUBSYSTEMS_HPP
#define LRPSUBSYSTEMS_HPP

#include <ros/ros.h>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <map>
#include <ctime>
#include <string>
#include <sstream>
#include <cstdlib>
#include <stdexcept>
#include <stdlib.h>
#include "std_msgs/String.h"	

namespace gazebo {

	class LRPSUBSYSTEMS;	
	class LRPSUBSYSTEMS
	{
		public:
		friend class RPTOR;
		enum GimbalState			{ NOT_READY = 0, READY = 1 };
		enum OnOff						{ OFF = 0, ON = 1 };
		enum Payload					{ Prospecting = 1 };
		enum StartStop				{ Stop = 0, Start = 1 };
		enum Kin							{ Hold = 0, Cart = 1 };
		enum KinSusp					{ sHold = 0 };
		enum RoverMode 				{ Stationary = 0 };
		enum Data							{ NOT_FLOWING = 0, FLOWING = 1 };		
		 LRPSUBSYSTEMS();
		~LRPSUBSYSTEMS() { }					

    ///////////////////////////////////////////////////////////       
		static int navcamDataState2int(LRPSUBSYSTEMS::Data gs);
		static LRPSUBSYSTEMS::Data int2navcamDataState(int i);
		static std::string int2navcamDataStateString(int i);
    ///////////////////////////////////////////////////////////       
		static int gimbalState2int(LRPSUBSYSTEMS::GimbalState gs);
		static LRPSUBSYSTEMS::GimbalState int2gimbalState(int i);
		static std::string int2gimbalStateString(int i);
    ///////////////////////////////////////////////////////////       
		static int cameraState2int(LRPSUBSYSTEMS::OnOff cs);
		static LRPSUBSYSTEMS::OnOff int2cameraState(int i);
		static std::string int2cameraStateString(int i);
    ///////////////////////////////////////////////////////////       
		static int payloadState2int(LRPSUBSYSTEMS::Payload ps);
		static LRPSUBSYSTEMS::Payload int2payloadState(int i);
		static std::string int2payloadStateString(int i);
    ///////////////////////////////////////////////////////////       
		static int mobState2int(LRPSUBSYSTEMS::StartStop ps); 
		static LRPSUBSYSTEMS::StartStop int2mobState(int i);
		static std::string int2mobStateString(int i);
    ///////////////////////////////////////////////////////////       
		static int kinDriveState2int(LRPSUBSYSTEMS::Kin kds);
		static LRPSUBSYSTEMS::Kin int2kinDriveState(int i);
		static std::string int2kinDriveStateString(int i);		
    ///////////////////////////////////////////////////////////       
		static int kinSuspState2int(LRPSUBSYSTEMS::KinSusp kss); 
		static LRPSUBSYSTEMS::KinSusp int2kinSuspState(int i);
		static std::string int2kinSuspStateString(int i);
    ///////////////////////////////////////////////////////////       
		static int hazcamState2int(LRPSUBSYSTEMS::OnOff hcs);  
		static LRPSUBSYSTEMS::OnOff int2hazcamState(int i);
		static std::string int2hazcamStateString(int i);
    ///////////////////////////////////////////////////////////       
		static int wMotors2int(LRPSUBSYSTEMS::OnOff ms);  // .hpp
		static LRPSUBSYSTEMS::OnOff int2wMotors(int i);
		static std::string int2wMotorsString(int i);
    ///////////////////////////////////////////////////////////       
		static int mode2int(LRPSUBSYSTEMS::RoverMode md);  // .hpp
		static LRPSUBSYSTEMS::RoverMode int2mode(int i);
		static std::string int2modeString(int i);
    ///////////////////////////////////////////////////////////       
	};		
      

	int LRPSUBSYSTEMS::navcamDataState2int(LRPSUBSYSTEMS::Data ncds) {
	return static_cast<int>(ncds);
	}

	LRPSUBSYSTEMS::Data LRPSUBSYSTEMS::int2navcamDataState(int i) {
		if (i < NOT_FLOWING || i > FLOWING ) {
			return NOT_FLOWING;
		}
		return static_cast<LRPSUBSYSTEMS::Data>(i);
	}

	std::string LRPSUBSYSTEMS::int2navcamDataStateString(int i) {

		if (i==0)
			return "NOT_FLOWING";
			
		else if (i==1)
			return "FLOWING";

		else
			return "NOT_FLOWING";
	}
	//////////////////////////////////////////////////////////////////

	int LRPSUBSYSTEMS::gimbalState2int(LRPSUBSYSTEMS::GimbalState gs) {
	return static_cast<int>(gs);
	}

	LRPSUBSYSTEMS::GimbalState LRPSUBSYSTEMS::int2gimbalState(int i) {
		if (i < NOT_READY || i > READY ) {
			return NOT_READY;
		}
		return static_cast<LRPSUBSYSTEMS::GimbalState>(i);
	}

	std::string LRPSUBSYSTEMS::int2gimbalStateString(int i) {

		if (i==0)
			return "OFF";
			
		else if (i==1)
			return "ON";
			
		else if (i==2)
			return "STOWED";

		else if (i==3)
			return "DEPLOYED";

		else
			return "OFF";
	}
	//////////////////////////////////////////////////////////////////

	int LRPSUBSYSTEMS::cameraState2int(LRPSUBSYSTEMS::OnOff cs) {
	return static_cast<int>(cs);
	}

	LRPSUBSYSTEMS::OnOff LRPSUBSYSTEMS::int2cameraState(int i) {
		if (i < OFF || i > ON ) {
			return OFF;
		}
		return static_cast<LRPSUBSYSTEMS::OnOff>(i);
	}

	std::string LRPSUBSYSTEMS::int2cameraStateString(int i) {

		if (i==0)
			return "OFF";
			
		else if (i==1)
			return "ON";				
		
		else
			return "OFF";
	}
	//////////////////////////////////////////////////////////////////

	int LRPSUBSYSTEMS::payloadState2int(LRPSUBSYSTEMS::Payload ps) {       
	return static_cast<int>(ps);
	}

	LRPSUBSYSTEMS::Payload LRPSUBSYSTEMS::int2payloadState(int i) {			
		return static_cast<LRPSUBSYSTEMS::Payload>(i);
	}

	std::string LRPSUBSYSTEMS::int2payloadStateString(int i) {

		if (i==0)
			return "Off";
			
		else if (i==1)
			return "Prospecting";				
		
		else
			return "Off";
	}
	//////////////////////////////////////////////////////////////////

	int LRPSUBSYSTEMS::mobState2int(LRPSUBSYSTEMS::StartStop ps) {        
	return static_cast<int>(ps);
	}

	LRPSUBSYSTEMS::StartStop LRPSUBSYSTEMS::int2mobState(int i) {
		if (i < Stop || i > Start ) {
			return Stop;
		}
		return static_cast<LRPSUBSYSTEMS::StartStop>(i);
	}

	std::string LRPSUBSYSTEMS::int2mobStateString(int i) {

		if (i==0)
			return "Off";
			
		else if (i==1)
			return "Prospecting";				
		
		else
			return "Off";
	}
	//////////////////////////////////////////////////////////////////

	int LRPSUBSYSTEMS::kinDriveState2int(LRPSUBSYSTEMS::Kin kds) {
	return static_cast<int>(kds);
	}

	LRPSUBSYSTEMS::Kin LRPSUBSYSTEMS::int2kinDriveState(int i) {
		if (i < Hold || i > Cart ) {
			return Hold;
		}
		return static_cast<LRPSUBSYSTEMS::Kin>(i);
	}

	std::string LRPSUBSYSTEMS::int2kinDriveStateString(int i) {

		if (i==0)
			return "Hold";
			
		else if (i==1)
			return "Cart";				
		
		else
			return "Hold";
	}
	//////////////////////////////////////////////////////////////////

	int LRPSUBSYSTEMS::kinSuspState2int(LRPSUBSYSTEMS::KinSusp kss) {
	return static_cast<int>(kss);
	}

	LRPSUBSYSTEMS::KinSusp LRPSUBSYSTEMS::int2kinSuspState(int i) {
			return sHold;
	}

	std::string LRPSUBSYSTEMS::int2kinSuspStateString(int i) {
			return "Hold";
	}
	//////////////////////////////////////////////////////////////////

	int LRPSUBSYSTEMS::hazcamState2int(LRPSUBSYSTEMS::OnOff hcs) {
	return static_cast<int>(hcs);
	}

	LRPSUBSYSTEMS::OnOff LRPSUBSYSTEMS::int2hazcamState(int i) {
		if (i < OFF || i > ON ) {
			return OFF;
		}
		return static_cast<LRPSUBSYSTEMS::OnOff>(i);
	}

	std::string LRPSUBSYSTEMS::int2hazcamStateString(int i) {

		if (i==0)
			return "Off";
			
		else if (i==1)
			return "On";				
		
		else
			return "Off";
	}
	///////////////////////////////////////////

	int LRPSUBSYSTEMS::wMotors2int(LRPSUBSYSTEMS::OnOff ms) {
	return static_cast<int>(ms);
	}

	LRPSUBSYSTEMS::OnOff LRPSUBSYSTEMS::int2wMotors(int i) {
	if (i < OFF || i > ON ) {
		return OFF;
	}
	return static_cast<LRPSUBSYSTEMS::OnOff>(i);
	}
	////////////////////////////////////////////////////////

	std::string LRPSUBSYSTEMS::int2wMotorsString(int i) {

		if (i==0)
			return "Off";
			
		else if (i==1)
			return "On";				
		
		else
			return "Off";
	}
	///////////////////////////////////////////////////
	int LRPSUBSYSTEMS::mode2int(LRPSUBSYSTEMS::RoverMode md) {        
	return static_cast<int>(md);
	}

	LRPSUBSYSTEMS::RoverMode LRPSUBSYSTEMS::int2mode(int i) {
			return Stationary;
	}

	std::string LRPSUBSYSTEMS::int2modeString(int i) {
			return "Stationary";
	}
}//end gazebo
#endif